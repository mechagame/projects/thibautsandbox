﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ArmorSystem
{
    public class ASHealthManager : MonoBehaviour
    {
        [SerializeField] private ASArmorManager armorManager;
        [SerializeField] private int maxHealth;

        [Serializable] public class OnDamageTaken : UnityEvent<int> { }
        public OnDamageTaken onDamageTaken;

        public UnityEvent onDeath;
        private int currentHealth;

        private void Start()
        {
            currentHealth = maxHealth;
            onDamageTaken?.Invoke(currentHealth);
        }

        public void TakeDamage(int damageTaken)
        {
            armorManager.CalculateDamage(damageTaken, out int remainingDamage);
            currentHealth -= remainingDamage;
            onDamageTaken?.Invoke(currentHealth);
            if (currentHealth > 0) { return; }
            onDeath?.Invoke();
        }
    }
}
