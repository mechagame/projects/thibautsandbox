﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArmorSystem
{
    public class ASArmorManager : MonoBehaviour
    {
        [SerializeField] private ASArmorPool armorPool;
        [SerializeField] private string emptyArmorName;
        private ASArmorData currentArmor;
        private int currentArmorValue;
        private float remainingCooldown;

        public event Action<ASArmorData, int> onArmorChange;

        private void Start()
        {
            EquipNewArmor(emptyArmorName);
        }

        private void Update()
        {
            Cooldown();
            UpdateArmor();
        }

        private void UpdateArmor()
        {
            currentArmor.UpdateArmor();
        }

        private void Cooldown()
        {
            remainingCooldown -= Time.deltaTime;
            remainingCooldown = Mathf.Max(remainingCooldown, 0);
        }

        public void EquipNewArmor(string armorName)
        {
            if (currentArmor != null)
            {
                currentArmor.Destroy();
            }
            ASArmorData newArmor = armorPool.GetArmorPool[armorName];
            currentArmor = newArmor;
            currentArmorValue = currentArmor.GetArmorValue;
            currentArmor.Pickup();
            onArmorChange?.Invoke(currentArmor, currentArmorValue);
        }

        public void CalculateDamage(int damageTaken, out int remainingDamage)
        {
            remainingDamage = 0;
            currentArmorValue -= damageTaken;
            currentArmor.TakeDamage();
            onArmorChange?.Invoke(currentArmor, currentArmorValue);
            if (currentArmorValue > 0) { return; }
            remainingDamage = Mathf.Abs(currentArmorValue);
            EquipNewArmor(emptyArmorName);
        }

        public void ExecuteAction()
        {
            if (remainingCooldown > 0) { return; }
            currentArmor.ExecuteAction();
            remainingCooldown = currentArmor.GetActionCooldown;
        }
    }
}
