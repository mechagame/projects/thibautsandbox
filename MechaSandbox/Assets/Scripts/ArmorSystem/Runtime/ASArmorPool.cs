﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArmorSystem
{
    public class ASArmorPool : MonoBehaviour
    {
        [SerializeField] private List<ASArmorData> armorDatas = new List<ASArmorData>();

        private Dictionary<string, ASArmorData> armorPool = new Dictionary<string, ASArmorData>();
        public Dictionary<string, ASArmorData> GetArmorPool => armorPool;

        private void Awake()
        {
            for (int i = 0; i < armorDatas.Count; i++)
            {
                armorPool.Add(armorDatas[i].GetArmorName, armorDatas[i]);
            }
        }
    }
}
