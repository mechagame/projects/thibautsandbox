﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ArmorSystem
{
    public class ASArmorHealthDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI currentHealth, currentArmorValue, currentArmor;
        [SerializeField] private ASArmorManager armor;

        private void Start()
        {
            armor.onArmorChange += ArmorChangeHandler;
        }

        private void OnDestroy()
        {
            armor.onArmorChange -= ArmorChangeHandler;
        }

        public void ModifyHealth(int health)
        {
            currentHealth.text = health.ToString();
        }

        private void ArmorChangeHandler(ASArmorData armor, int armorValue)
        {
            currentArmor.text = armor.GetArmorName;
            currentArmorValue.text = armorValue.ToString();
        }

    }
}
