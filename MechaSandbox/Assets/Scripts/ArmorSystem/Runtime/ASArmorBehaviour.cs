﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArmorSystem
{
    public abstract class ASArmorBehaviour : MonoBehaviour
    {
        public virtual void OnPickup() { }
        public virtual void OnDamaged() { }
        public virtual void OnArmorActive() { }
        public virtual void OnExecuteAction() { }
        public virtual void OnDestroyed() { }
    }
}
