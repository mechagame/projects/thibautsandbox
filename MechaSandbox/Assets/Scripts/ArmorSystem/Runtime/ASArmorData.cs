﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArmorSystem
{
    public class ASArmorData : MonoBehaviour
    {
        [SerializeField] private GameObject armorItem;
        [SerializeField] private string armorName;
        public string GetArmorName => armorName;
        [SerializeField] private int armorValue;
        public int GetArmorValue => armorValue;
        [SerializeField] private float actionCooldown;
        public float GetActionCooldown => actionCooldown;
        [SerializeField] private List<ASArmorBehaviour> behaviours = new List<ASArmorBehaviour>();

        public void Pickup()
        {
            armorItem.SetActive(true);
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnPickup();
            }
        }

        public void TakeDamage()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnDamaged();
            }
        }

        public void UpdateArmor()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnArmorActive();
            }
        }

        public void ExecuteAction()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnExecuteAction();
            }
        }

        public void Destroy()
        {
            armorItem.SetActive(false);
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnDestroyed();
            }
        }

        private void OnValidate()
        {
            gameObject.name = armorName;
        }
    }
}
