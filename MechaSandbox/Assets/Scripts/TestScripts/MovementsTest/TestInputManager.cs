﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestInputManager : MonoBehaviour
{
    [System.Serializable]
    public class MovementEvent : UnityEvent<Vector3> { }
    [SerializeField]
    private MovementEvent movementEvent;

    [SerializeField] private UnityEvent onShootInput;

    [SerializeField] private UnityEvent onMeleeInput;

    [SerializeField] private UnityEvent onRangeSwapInput;
    [SerializeField] private UnityEvent onRangeThrowInput;

    [SerializeField] private UnityEvent onMeleeSwapInput;
    [SerializeField] private UnityEvent onMeleeThrowInput;

    [Serializable] public class WeaponLootEvent : UnityEvent<string> { };
    [SerializeField] private WeaponLootEvent onWeaponLoot;
    public string rangeWeapon;

    [Serializable] public class DamageTakenEvent : UnityEvent<int> { };
    [SerializeField] private DamageTakenEvent onDamageTaken;
    public int damageToTake;

    [Serializable] public class ArmorLootEvent : UnityEvent<string> { };
    [SerializeField] private ArmorLootEvent onArmorLoot;
    public string armorToLoot;


    private void Update()
    {
        MoveInputs();
        AttackInput();
        SwapWeaponsInput();
        ThrowInput();
        WeaponLootEventMethod();
        TakeDamage();
        LootArmor();
    }

    private void LootArmor()
    {
        if (Input.GetKeyDown(KeyCode.O))
            onArmorLoot?.Invoke(armorToLoot);
    }

    private void TakeDamage()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            onDamageTaken?.Invoke(damageToTake);
        }
    }

    private void WeaponLootEventMethod()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            onWeaponLoot?.Invoke(rangeWeapon);
        }
    }

    private void ThrowInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            onRangeThrowInput?.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            onMeleeThrowInput?.Invoke();
        }
    }

    private void SwapWeaponsInput()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            onRangeSwapInput?.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            onMeleeSwapInput?.Invoke();
        }
    }

    private void MoveInputs()
    {
        float X = Input.GetAxisRaw("Horizontal");
        float Z = Input.GetAxisRaw("Vertical");
        Vector3 movement = new Vector3(X, 0, Z);
        movementEvent?.Invoke(movement);
    }

    private void AttackInput()
    {
        if (Input.GetMouseButton(0))
        {
            onShootInput?.Invoke();
        }

        if (Input.GetMouseButtonDown(1))
        {
            onMeleeInput?.Invoke();
        }
    }
}
