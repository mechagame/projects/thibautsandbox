﻿using CharacterControllerCamera;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPush : MonoBehaviour
{
    public float force;
    public float deceleration;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FindObjectOfType<CCCPushMovement>().Push(Vector3.forward, force, deceleration);
        }
    }
}
