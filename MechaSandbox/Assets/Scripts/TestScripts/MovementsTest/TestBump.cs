﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestBump : MonoBehaviour
{
    [Serializable] private class OnBump : UnityEvent<Vector3, float, float> { };
    [SerializeField] private OnBump onBumpEvent;
    public float bumpForce;
    public float deceleration;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bumper"))
        {
            Vector3 bumpDirection = transform.position - other.gameObject.transform.position;
            Vector3 direction = new Vector3(bumpDirection.x, 0, bumpDirection.z);
            onBumpEvent?.Invoke(direction, bumpForce, deceleration);
        }
    }
}
