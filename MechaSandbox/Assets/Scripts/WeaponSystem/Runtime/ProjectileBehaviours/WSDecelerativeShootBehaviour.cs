﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSDecelerativeShootBehaviour : WSProjectileBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private float timeBeforeDeceleration;
        [SerializeField] private float decelerationForce;

        private float remainingTimeBeforeDeceleration;
        private float currentSpeed;

        public override void OnInitialize()
        {
            currentSpeed = speed;
            remainingTimeBeforeDeceleration = timeBeforeDeceleration;
        }

        public override void OnUpdate()
        {
            Move();
            DecelerationCooldown();
            Decelerate();
        }

        private void DecelerationCooldown()
        {
            remainingTimeBeforeDeceleration -= Time.deltaTime;
        }

        private void Decelerate()
        {
            if(remainingTimeBeforeDeceleration > 0) { return; }

            currentSpeed = Mathf.MoveTowards(currentSpeed, 0, decelerationForce * Time.deltaTime);

            if(currentSpeed <= 0) { gameObject.SetActive(false); }
        }

        private void Move()
        {
            gameObject.transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);
        }
    }
}
