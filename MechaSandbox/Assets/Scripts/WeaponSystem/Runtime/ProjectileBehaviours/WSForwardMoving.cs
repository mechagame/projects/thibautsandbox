﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [CreateAssetMenu(menuName = "WeaponSystem/ProjectileBehaviour/ForwardMoving")]
    public class WSForwardMoving : WSProjectileBehaviour
    {
        [SerializeField] private float movementSpeed;

        public override void OnUpdate()
        {
            pGameObject.transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
        }
    }
}
