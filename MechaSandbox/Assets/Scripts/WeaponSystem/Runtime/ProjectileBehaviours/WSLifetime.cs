﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [CreateAssetMenu (menuName = "WeaponSystem/ProjectileBehaviour/Lifetime")]
    public class WSLifetime : WSProjectileBehaviour
    {
        [SerializeField] private float lifetime;
        private float remainingLifetime;

        public override void OnInitialize()
        {
            remainingLifetime = lifetime;
        }

        public override void OnUpdate()
        {
            remainingLifetime -= Time.deltaTime;
            if(remainingLifetime <= 0)
            {
                pGameObject.SetActive(false);
            }
        }
    }
}
