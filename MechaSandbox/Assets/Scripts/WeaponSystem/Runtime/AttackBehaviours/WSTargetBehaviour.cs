﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSTargetBehaviour : WSWeaponBehaviour
    {
        private LineRenderer lineRenderer;
        private Camera cam;
        private Vector3 targetPosition;
        [SerializeField] private Gradient lineGradient;
        [SerializeField] private Transform weaponPosition;
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private float characterHeight;
        [SerializeField] private Vector2 lineWidth;
        [SerializeField] private Material lineMaterial;

        public override void OnInitialize()
        {

            if(lineRenderer != null) { lineRenderer.enabled = true; return; }
            cam = Camera.main;
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.colorGradient = lineGradient;
            lineRenderer.startWidth = lineWidth.x;
            lineRenderer.endWidth = lineWidth.y;
            lineRenderer.material = lineMaterial;
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }

        public override void OnWeaponActive()
        {
            lineRenderer.SetPosition(0, weaponPosition.position);

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, groundLayer))
            {
                targetPosition = hitInfo.point;
            }
            lineRenderer.SetPosition(1, targetPosition);
        }

        public override void OnDeactivate()
        {
            if(lineRenderer == null) { return; }
            lineRenderer.enabled = false;
        }
    }
}
