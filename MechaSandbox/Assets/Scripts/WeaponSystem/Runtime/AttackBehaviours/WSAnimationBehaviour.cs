﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSAnimationBehaviour : WSWeaponBehaviour
    {
        [SerializeField] private Animator anim;
        [SerializeField] private string triggerName;

        public override void OnExecute()
        {
            anim.SetTrigger(triggerName);
        }
    }
}
