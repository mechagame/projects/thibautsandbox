﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSWeaponData : MonoBehaviour
    {
        [SerializeField] private string weaponName;
        public string GetWeaponName => weaponName;

        [SerializeField] private GameObject weaponModel;
        public GameObject GetWeaponModel => weaponModel;

        [SerializeField] private float cooldown;
        public float GetCooldown => cooldown;

        [SerializeField] private int ammo;
        public int GetAmmo => ammo;

        [SerializeField] private List<WSWeaponBehaviour> behaviours;
        public List<WSWeaponBehaviour> GetBehaviours => behaviours;

        public void Initialize()
        {
            weaponModel.SetActive(true);
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnInitialize();
            }
        }

        public void UpdateBehaviour()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnWeaponActive();
            }
        }

        public void Attack()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnExecute();
            }
        }

        public void DeactivateWeapon()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnDeactivate();
            }
            weaponModel.SetActive(false);
        }

        private void OnValidate()
        {
            gameObject.name = weaponName;
        }
    }
}
