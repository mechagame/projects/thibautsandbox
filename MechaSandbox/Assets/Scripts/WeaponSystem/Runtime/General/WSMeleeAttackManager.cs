﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSMeleeAttackManager : WSAttackManager
    {
        private bool canCollide = true;

        protected override void RemoveAmmo() { return; }

        private void Awake() => WSMeleeCollision.onCollided += OnCollidedHandler;
        private void OnDestroy() => WSMeleeCollision.onCollided -= OnCollidedHandler;

        private void OnCollidedHandler()
        {
            if (!canCollide) return;
            RemoveBullet();
            canCollide = false;
            StartCoroutine(CollisionCooldown(remainingCooldown));
        }

        private IEnumerator CollisionCooldown(float time)
        {
            yield return new WaitForSeconds(time);
            canCollide = true;
        }
    }
}
