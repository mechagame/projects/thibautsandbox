﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace WeaponSystem
{
    public class WSInventoryDisplay : MonoBehaviour
    {
        [SerializeField] private List<TextMeshProUGUI> weapons;
        [SerializeField] private List<TextMeshProUGUI> bullets;
        [SerializeField] private WSAttackManager attackManager;

        private void Awake()
        {
            attackManager.onInventoryChange += WeaponChangedHandler;
        }

        private void OnDestroy()
        {
            attackManager.onInventoryChange -= WeaponChangedHandler;
        }

        private void WeaponChangedHandler(List<WSInventoryWeapon> inventory)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                string weaponName = inventory[i].weapon.GetWeaponName;
                weapons[i].text = weaponName;

                int ammo = inventory[i].ammo;
                bullets[i].text = ammo < 0 ? "Infinite" : ammo.ToString();
            }
        }
    }
}
