﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSInventoryWeapon
    {
        public WSWeaponData weapon;
        public int ammo;

        public WSInventoryWeapon(WSWeaponData weapon)
        {
            if(weapon == null) { return; }
            this.weapon = weapon;
            this.ammo = weapon.GetAmmo;
        }
    }
}
