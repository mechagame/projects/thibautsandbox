﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public abstract class WSAttackManager : MonoBehaviour
    {
        public event Action<List<WSInventoryWeapon>> onInventoryChange;

        private List<WSInventoryWeapon> inventory = new List<WSInventoryWeapon>();

        [SerializeField] private WSAttackPool attackPool;
        [SerializeField] private int inventorySize = 2;
        [SerializeField] private string emptyWeapon;
        [SerializeField] private string firstWeapon;
        [SerializeField] private string secondWeapon;

        protected float remainingCooldown = 0;

        protected WSInventoryWeapon currentWeapon => inventory[0];

        private void Start()
        {
            AddWeapon(firstWeapon);
            AddWeapon(secondWeapon);
        }

        private void Update()
        {
            UpdateCurrentWeapon();
            Cooldown();
        }

        #region AttackManager

        public void Attack()
        {
            if (remainingCooldown != 0) { return; }
            if (currentWeapon.ammo == 0) { return; }
            currentWeapon.weapon.Attack();
            RemoveAmmo();
            remainingCooldown = currentWeapon.weapon.GetCooldown;
            onInventoryChange?.Invoke(inventory);
        }

        protected abstract void RemoveAmmo();

        protected void RemoveBullet()
        {
            currentWeapon.ammo--;
            onInventoryChange?.Invoke(inventory);
        }

        private void Cooldown()
        {
            remainingCooldown -= Time.deltaTime;
            remainingCooldown = Mathf.Max(remainingCooldown, 0);
        }

        private void UpdateCurrentWeapon()
        {
            currentWeapon.weapon.UpdateBehaviour();
        }

        #endregion

        #region Inventory
        public void AddWeapon(string weaponName)
        {
            WSWeaponData weapon = attackPool.GetWeaponsPool[weaponName];
            WSInventoryWeapon newWeapon = new WSInventoryWeapon(weapon);

            if (inventory.Count < inventorySize)
            {
                inventory.Add(newWeapon);
                currentWeapon.weapon.DeactivateWeapon();
                inventory.Reverse();
                currentWeapon.weapon.Initialize();
            }
            else
            {
                currentWeapon.weapon.DeactivateWeapon();
                inventory[0] = newWeapon;
                currentWeapon.weapon.Initialize();
            }

            onInventoryChange?.Invoke(inventory);
        }

        public void RemoveCurrentWeapon()
        {
            currentWeapon.weapon.DeactivateWeapon();
            AddWeapon(emptyWeapon);
            inventory.Reverse();
            currentWeapon.weapon.Initialize();
            onInventoryChange?.Invoke(inventory);
        }

        public void SwapWeapons()
        {
            currentWeapon.weapon.DeactivateWeapon();
            inventory.Reverse();
            currentWeapon.weapon.Initialize();
            onInventoryChange?.Invoke(inventory);
        }
        #endregion
    }
}
